import tkinter as tk
from tkinter import ttk
from requests import get
import math


class App(tk.Tk):
# Tworzenie okna    
    def __init__(apka):
        super().__init__()
        apka.geometry("450x200")
        apka.title("Kalkulator walutowy")
        apka.waluty=('USD','GBP','EUR','CHF','JPY','TRY','CNY')
        apka.option_var=tk.StringVar(apka)
        apka.create_wigets()
# Tworzenie widgetow        
    def create_wigets(apka):
        paddings={'padx':5,'pady':5}
#label wpisz ilosc        
        label = ttk.Label(apka,  text='Wpisz ilość: ')
        label.grid(column=0,row=0,stick=tk.W, **paddings)
# entry do ilosci        
        apka.ilosc=ttk.Entry(apka, width=30)
        apka.ilosc.focus()
        apka.ilosc.grid(column=1,row=0,sticky=tk.W,**paddings)
# menu wyboru
        apka.menu_wyboru=ttk.OptionMenu(
            apka,
            apka.option_var,
            apka.waluty[0],
            *apka.waluty,
            #command=apka.option_changed
        )
        apka.menu_wyboru.grid(column=2,row=0,stick=tk.W, **paddings)
#label do daty 
        label_data=ttk.Label(apka,text='Podaj date YYYY-MM-DD: ')
        label_data.grid(column=0,row=1,stick=tk.W, **paddings)

# Entry z datą
        apka.wpisz_date=ttk.Entry(apka, width=30)
        apka.wpisz_date.focus()
        apka.wpisz_date.grid(column=1,row=1,stick=tk.W, **paddings)
# label z wynikiem 
        apka.wynik=ttk.Label(apka)
        apka.wynik.grid(column=0,row=2,stick=tk.W, **paddings)
        apka.wynik['text']= f' 1 {apka.option_var.get()} wynosi : '
#label z wynikiem 2
        apka.wynik2=ttk.Label(apka)
        apka.wynik2.grid(column=1,row=2,stick=tk.W, **paddings)
        apka.wynik2['text']= ''
#przycisk zamknij
        ttk.Button(apka,text="Quit", command=apka.destroy).grid(column=1,row=3,stick=tk.W, **paddings)
#przycisk przeliczający waluty  
        ttk.Button(apka,text='Przelicz',command=apka.przeliczanie).grid(column=2,row=1,stick=tk.W)
#fukcja do przycisku
    
    def przeliczanie(apka, *args):
        def multiply(x,y):
            return x*y;
        data_skrypt=apka.wpisz_date.get()
        kurs=get(f"http://api.nbp.pl/api/exchangerates/rates/a/{apka.option_var.get()}/{data_skrypt}/?format=json")
        if kurs.ok:
            dane=kurs.json()
            kurs_wynik=float(dane['rates'][0]['mid'])
            ilosc_waluty=float(apka.ilosc.get())
    #def option_changed(apka, *args):
            apka.wynik['text']= f' {apka.ilosc.get()} {apka.option_var.get()} wynosi : '
            apka.wynik2['text']=f'{multiply(ilosc_waluty,kurs_wynik)} PLN.'
        else:
            apka.wynik['text']='Brak danych'
if __name__ == "__main__":
    app = App()
    app.mainloop()